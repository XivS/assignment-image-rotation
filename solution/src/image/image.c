#include "image.h"

image rotate_image(image *img){
	image rotated_img = {0};
	rotated_img.width = img->height;
	rotated_img.height = img->width;
	pixel **array_old = image_to_array(img);
	pixel **array_new = allocate_pixel_array(rotated_img.width, rotated_img.height);

	
	for(size_t i = 0; i < (size_t)rotated_img.height; i++){
		for(size_t j = 0; j < (size_t)rotated_img.width; j++){
			array_new[i][j] = array_old[img->height-j - 1][i];
		}
	}
	
	rotated_img.data = array_to_data(array_new, rotated_img.width, rotated_img.height);
	free_array(array_old, img->height);
	free_array(array_new, rotated_img.height);
	return rotated_img;
	
}

pixel** image_to_array(image *img){
	
	pixel **array = allocate_pixel_array(img->width, img->height);		
	for(size_t i = 0; i < (size_t)img->height; i++){
		for(size_t j = 0; j < (size_t)img->width; j++){

			array[i][j] = img->data[((size_t)(img->width) * i + j) ];
			
		}
	}
	return array;
}

pixel* array_to_data(pixel **array, uint64_t width, uint64_t  height){
	if(width > 0 && height > 0){
	pixel *data = malloc(sizeof(pixel) * width * height);
	if (data != NULL){
		for(size_t i = 0; i < height; i++){
			for(size_t j = 0; j < width; j++){
				data[((size_t)(width) * i + j)] = array[i][j];
			}
		}
	}
	return data;
	} else{
		return NULL;
	}
}

void free_array(pixel** array, uint64_t height){
	for(size_t i = 0; i < (size_t)height; i++){
		free(array[i]);
	}
	free(array);
}

pixel** allocate_pixel_array(size_t width, size_t height){
	if (height > 0 && width > 0){
	pixel **array = (pixel**)malloc(height * sizeof(pixel*));
	if (array == NULL) {
		free(array);
		return NULL;
	}
	for (size_t i = 0; i < height; i++){
		
		array[i] = (pixel*)calloc(1, width * sizeof(pixel));
		
	}
	return array;
	}
	else{
		return NULL;
	}
}
