#ifndef IMAGEH
#define IMAGEH
#include <stdint.h>
#include <stdio.h>
#include <malloc.h>

typedef struct{ uint8_t b, g, r; } pixel;
typedef struct  {
	uint64_t width, height;
	pixel* data;
	
}image;
image rotate_image(image *img);
pixel** image_to_array(image *img);
pixel* array_to_data(pixel **array, uint64_t width, uint64_t  height);
void free_array(pixel** array, uint64_t height);
pixel** allocate_pixel_array(size_t width, size_t height);
#endif
