
#include "io/ioutils.h"
#include "bmp/bmputils.h"
#include "image/image.h"
#include <stdio.h>
void debug(char *in, char *out){
	FILE *f = open_file(in, "rb");
	FILE *fout = open_file(out, "rb");
	bmp_header h_orig = {0};
	bmp_read_header(f, &h_orig);
	bmp_header h_new = {0};
	bmp_read_header(fout, &h_new);
	close_file(f);
	close_file(fout);
	
	
	printf("%s", "\nbfType: ");
	printf("%hu", h_orig.bfType);
	printf("%s", "   ");
	printf("%hu", h_new.bfType);
	
	printf("%s", "\nbfileSize: ");
	printf("%u", h_orig.bfileSize);
	printf("%s", "   ");
	printf("%u", h_new.bfileSize);
	
	printf("%s", "\nbfReserved: ");
	printf("%u", h_orig.bfReserved);
	printf("%s", "   ");
	printf("%u", h_new.bfReserved);
	
	printf("%s", "\nbOffBits: ");
	printf("%u", h_orig.bOffBits);
	printf("%s", "   ");
	printf("%u", h_new.bOffBits);
	
	printf("%s", "\nbiSize: ");
	printf("%u", h_orig.biSize);
	printf("%s", "   ");
	printf("%u", h_new.biSize);
	
	printf("%s", "\nbiWidth: ");
	printf("%u", h_orig.biWidth);
	printf("%s", "   ");
	printf("%u", h_new.biWidth);
	
	printf("%s", "\nbiHeight: ");
	printf("%u", h_orig.biHeight);
	printf("%s", "   ");
	printf("%u", h_new.biHeight);
	
	printf("%s", "\nbiPlanes: ");
	printf("%u", h_orig.biPlanes);
	printf("%s", "   ");
	printf("%u", h_new.biPlanes);
	
	printf("%s", "\nbiBitCount: ");
	printf("%u", h_orig.biBitCount);
	printf("%s", "   ");
	printf("%u", h_new.biBitCount);
	
	printf("%s", "\nbiCompression: ");
	printf("%u", h_orig.biCompression);
	printf("%s", "   ");
	printf("%u", h_new.biCompression);
	
	printf("%s", "\nbiSizeImage: ");
	printf("%u", h_orig.biSizeImage);
	printf("%s", "   ");
	printf("%u", h_new.biSizeImage);
	
	printf("%s", "\nbiXPelsPerMeter: ");
	printf("%u", h_orig.biXPelsPerMeter);
	printf("%s", "   ");
	printf("%u", h_new.biXPelsPerMeter);
	
	printf("%s", "\nbiYPelsPerMeter: ");
	printf("%u", h_orig.biYPelsPerMeter);
	printf("%s", "   ");
	printf("%u", h_new.biYPelsPerMeter);
	
	printf("%s", "\nbiClrUsed: ");
	printf("%u", h_orig.biClrUsed);
	printf("%s", "   ");
	printf("%u", h_new.biClrUsed);
	
	printf("%s", "\nbiClrImportant: ");
	printf("%u", h_orig.biClrImportant);
	printf("%s", "   ");
	printf("%u", h_new.biClrImportant);
}
int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
	if(argc < 3) fprintf(stderr, "Too few arguments\n");
	
	FILE *f = open_file(argv[1], "rb");
	bmp_header h_orig = {0};
	bmp_read_header(f, &h_orig);
	image img = {0};
	from_bmp(f, &img);
	FILE *fout = open_file(argv[2], "wb");
	image n = rotate_image(&img);
	to_bmp(fout, &n);
	close_file(fout);
	debug(argv[1], argv[2]);
	

    return 0;
}
