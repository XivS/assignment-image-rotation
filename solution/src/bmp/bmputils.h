#ifndef BMPUTILSH
#define BMPUTILSH
#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include "../image/image.h"
#pragma pack(push, 1)
typedef struct 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;

}bmp_header;
#pragma pack(pop)


typedef enum {
	READ_ERR_INVALID_HEADER,
	READ_ERR_NULL_PTR,
	READ_ERR_MALLOC_NULL,
	READ_SUCCESS
}bmp_read_status;
typedef enum {
	WRITE_ERR_NULL_PTR,
	WRITE_ERR_MALLOC_NULL,
	WRITE_ERR_DATA_NULL,
	WRITE_SUCCESS
}bmp_write_status;
bmp_read_status bmp_read_header(FILE *file, bmp_header *header);
size_t get_padding(size_t width);
bmp_read_status from_bmp(FILE *file, image* img);
bmp_read_status bmp_read_data(FILE *file, image* img, bmp_header *header);
void generate_bmp_header(image *img, bmp_header *header);
bmp_write_status to_bmp(FILE *file, image *img);
bmp_write_status bmp_write_to_file(FILE *file, bmp_header *header, image *img);
#endif
