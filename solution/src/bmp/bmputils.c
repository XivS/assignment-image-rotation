#include "bmputils.h"

#define BMP_TYPE_MAGIC 0x4D42
#define BMP_RESERVED_MAGIC 0
#define BMP_COMPRESSION_MAGIC 0
#define BMP_PIXEL_PER_METER_MAGIC 2834
#define BMP_PLANES_MAGIC 1
#define BMP_COLORS_MAGIC 0
#define BMP_HEADER_SIZE_MAGIC 40
#define DWORD_SIZE 4
#define COLOR_DEPTH 24 




bmp_read_status from_bmp(FILE *file, image* img){
	if (!file || !img) return READ_ERR_NULL_PTR;
	bmp_header header = {0};
	bmp_read_status status = bmp_read_header(file, &header);
	if (status != READ_SUCCESS) return status;
	img->width = header.biWidth;
	img->height = header.biHeight;
	status = bmp_read_data(file, img, &header);
	return status;
}

bmp_read_status bmp_read_header(FILE *file,  bmp_header *header){
	
	rewind(file);
	fread(header, sizeof(bmp_header), 1, file);
	if (header->bfType != BMP_TYPE_MAGIC) return READ_ERR_INVALID_HEADER;
	return READ_SUCCESS;
	
}
bmp_read_status bmp_read_data(FILE *file, image* img, bmp_header *header){
	pixel* data = malloc((size_t)(img->width * img->height * sizeof(pixel)));
	if (data == NULL) 
	{	free(data);
		return READ_ERR_MALLOC_NULL;
	}
	rewind(file);
	fseek(file, header->bOffBits, SEEK_SET);
	size_t padding = get_padding(img->width);
	for(size_t i = 0; i < img->height; i++){
		fread(data + (img->width) * i, sizeof(pixel) * (size_t)img->width, 1, file);
		fseek(file, padding, SEEK_CUR);
		
	}
	img->data = data;
	return READ_SUCCESS;
	
	
}
size_t get_padding(size_t width){
	
	size_t padding = DWORD_SIZE - (width * (COLOR_DEPTH / 8)) % DWORD_SIZE;
	return padding;
	
}

bmp_write_status to_bmp(FILE *file, image *img){
	if (!file || !img) return WRITE_ERR_NULL_PTR;
	bmp_header header = {0};
	generate_bmp_header(img, &header);
	return bmp_write_to_file(file, &header, img);
	

}

void generate_bmp_header(image *img, bmp_header *header){
	size_t padding = get_padding(img->width);
	
	size_t image_size = (sizeof(pixel) * (img->width) + padding) * img->height;
	
	
	header->bfType = BMP_TYPE_MAGIC;
	header->biBitCount = COLOR_DEPTH;
	header->biXPelsPerMeter = BMP_PIXEL_PER_METER_MAGIC;
	header->biYPelsPerMeter = BMP_PIXEL_PER_METER_MAGIC;
    header->bfileSize = image_size + BMP_RESERVED_MAGIC + sizeof(bmp_header);
    header->bfReserved = BMP_RESERVED_MAGIC;
    header->bOffBits = sizeof(bmp_header);
    header->biSize = BMP_HEADER_SIZE_MAGIC;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = BMP_COMPRESSION_MAGIC;
    header->biCompression = BMP_COMPRESSION_MAGIC;
    header->biSizeImage = image_size;
    header->biClrUsed = BMP_COLORS_MAGIC;
    header->biClrImportant = BMP_COLORS_MAGIC;
}

bmp_write_status bmp_write_to_file(FILE *file, bmp_header *header, image *img){
	size_t padding = get_padding(img->width);
	fwrite(header, sizeof(bmp_header), 1, file);
	fseek(file, header->bOffBits, SEEK_SET);
	printf("%s", "\n");
	printf("%u", header->bOffBits);
	uint8_t *padding_bits = calloc(1, padding);
	
	if (padding_bits == NULL) {
		free(padding_bits);
		return WRITE_ERR_MALLOC_NULL;
	}
		
	if (img->data == NULL) {
		free(padding_bits);
		return WRITE_ERR_DATA_NULL;
	}
	
	for (size_t i = 0; i < img->height; i++){
		
		fwrite(img->data + (header->biWidth) * i, (header->biWidth) * sizeof(pixel), 1, file);
		fwrite(padding_bits, padding, 1, file);
	}
	free(padding_bits);
	return WRITE_SUCCESS;
	
}
