#ifndef IOUTILSH
#define IOUTILSH

#include <stdbool.h>
#include <stdio.h>
FILE* open_file(char *name, char *mode);
bool close_file(FILE *file);
#endif
