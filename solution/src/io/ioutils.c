#include "ioutils.h"

FILE* open_file(char *name, char *mode) {
    
    FILE* file = fopen(name, mode);
    return file;
}

bool close_file(FILE *file) {
    return fclose(file);
}
